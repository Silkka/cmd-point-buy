#!/usr/bin/env python
from prompt_toolkit.application import Application
from prompt_toolkit.widgets import Button, TextArea, Box, Label
from prompt_toolkit.application.current import get_app
from prompt_toolkit.layout import HSplit, Layout, VSplit
from prompt_toolkit.key_binding import KeyBindings
from prompt_toolkit.key_binding.bindings.focus import (
    focus_next,
    focus_previous,
)
from point_buy_widgets import RangeBar, PointbuyBar
from math import floor
import pyperclip
attributes = {
    "points": 27,
    "str": 8,
    "dex": 8,
    "con": 8,
    "int": 8,
    "wis": 8,
    "cha": 8
}
def handle_attribute(attribute, operation):
    valid_change = False
    if operation == '+':
        if attributes[attribute] < 13 and attributes["points"] > 0:
            attributes[attribute] +=1
            attributes["points"] -=1
            valid_change = True
        elif attributes[attribute] < 15 and attributes["points"] > 1:
            attributes[attribute] +=1
            attributes["points"] -=2
            valid_change = True
    elif operation == '-':
        if attributes[attribute] > 13:
            attributes[attribute] -=1
            attributes["points"] +=2
            valid_change = True
        elif attributes[attribute] > 8:
            attributes[attribute] -=1
            attributes["points"] +=1
            valid_change = True
    points_label.text = point_label_text()
    return valid_change

def point_label_text():
    return "Pts " + "<" + floor((8/27)*attributes["points"])*'-' + str(attributes["points"]) +floor((8/27)* (27-attributes["points"]))*'-' + ">"

def exit_clicked():
    pyperclip.copy(
        str(attributes['str']) + "\n" + 
        str(attributes['dex']) + "\n" + 
        str(attributes['con']) + "\n" + 
        str(attributes['int']) + "\n" + 
        str(attributes['wis']) + "\n" + 
        str(attributes['cha'])  
    )
    get_app().exit()


#range_str = RangeBar(attributes['str'],8,15,text="Str ")
points_label = Label(text=point_label_text())
range_str = PointbuyBar(attributes['str'], 8, 15, "Str ", "str", handle_attribute)
range_dex = PointbuyBar(attributes['dex'],8,15,"Dex ",'dex', handle_attribute)
range_con = PointbuyBar(attributes['con'],8,15,"Con ",'con', handle_attribute)
range_int = PointbuyBar(attributes['int'],8,15,"Int ",'int', handle_attribute)
range_wis = PointbuyBar(attributes['wis'],8,15,"Wis ",'wis', handle_attribute)
range_cha = PointbuyBar(attributes['cha'],8,15,"Cha ",'cha', handle_attribute)

button_exit = Button("Exit and copy", handler=exit_clicked)

root_container = Box(
    HSplit([
        points_label,
        range_str,
        range_dex,
        range_con,
        range_int,
        range_wis,
        range_cha,
        button_exit
    ])
)

layout = Layout(
    container=root_container,
    focused_element=range_str
)

kb = KeyBindings()
kb.add('down')(focus_next)
kb.add('up')(focus_previous)


application = Application(
    layout=layout,
    key_bindings=kb,
    full_screen=True)

def main():
    application.run()


if __name__ == '__main__':
    main()