from prompt_toolkit.layout import FormattedTextControl,Window, WindowAlign
from prompt_toolkit.key_binding.key_bindings import KeyBindings

class RangeBar(object):
    def __init__(self, value, min_value, max_value,text='', on_change=None, step=1):
        assert min_value <= value 
        assert value <= max_value
        assert on_change is None or callable(on_change)


        self.value = value
        self.min_value = min_value
        self.max_value = max_value
        self.step = step
        self.text = text
        self.on_change = on_change

        self.control = FormattedTextControl(
            text=self._get_bar_text(),
            focusable=True,
            key_bindings=self._get_key_bindings()
        )
        self.window = Window(
            self.control,
            align=WindowAlign.LEFT,
            dont_extend_width=True,
            dont_extend_height=True
        )


    def _get_bar_text(self):
        return self.text +  "<" + "-"*(self.value - self.min_value) + str(self.value) + "-"*(self.max_value - self.value) + ">"
    
    def _get_key_bindings(self):
        kb = KeyBindings()

        @kb.add('left')
        def _(event):
            if self.min_value < self.value:
                self.value -=self.step
                self.control.text = self._get_bar_text()

        @kb.add('right')
        def _(event):
                if self.value < self.max_value:
                    self.value += self.step
                    self.control.text = self._get_bar_text()
        return kb
    def __pt_container__(self):
        return self.window


class PointbuyBar(RangeBar):
    def __init__(self, value, min_value, max_value, text, attribute, validator):
        super().__init__(value, min_value, max_value, text=text)
        self.attribute = attribute
        self.validator = validator
    
    def _get_key_bindings(self):
        kb = KeyBindings()

        @kb.add('left')
        def _(event):
            if self.validator(self.attribute, "-"):
                self.value -=self.step
                self.control.text = self._get_bar_text()

        @kb.add('right')
        def _(event):
            if self.validator(self.attribute, "+"):
                self.value += self.step
                self.control.text = self._get_bar_text()
        return kb